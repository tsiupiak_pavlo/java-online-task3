package task2;

public class Triangle extends Shape {
    int first_side, second_side, third_side;

    public Triangle(int first_side, int second_side, int third_side) {
        this.first_side = first_side;
        this.second_side = second_side;
        this.third_side = third_side;
    }

    public Triangle(String color, int first_side, int second_side, int third_side) {
        super(color);
        this.first_side = first_side;
        this.second_side = second_side;
        this.third_side = third_side;
    }

    @Override
    public double area() {
        double s = (first_side + second_side + third_side) / 2;
        return Math.sqrt(s * (s - first_side) * (s - second_side) * (s - third_side));
    }

    public int getFirst_side() {
        return first_side;
    }

    public void setFirst_side(int first_side) {
        this.first_side = first_side;
    }

    public int getSecond_side() {
        return second_side;
    }

    public void setSecond_side(int second_side) {
        this.second_side = second_side;
    }

    public int getThird_side() {
        return third_side;
    }

    public void setThird_side(int third_side) {
        this.third_side = third_side;
    }
}
