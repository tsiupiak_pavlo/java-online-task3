package task2;

public class Main {

    public static void main(String[] args) {

        Rectangle rectangle = new Rectangle("Green", 5, 5);
        System.out.println("Rectangle"
                + "\nColor: " + rectangle.getColor()
                + "\nWidth: " + rectangle.getWidth() + " Length: " + rectangle.getLength()
                + "\nArea: " + rectangle.area());

        Triangle triangle = new Triangle(3, 4, 5);
        System.out.println("Triangle"
                + "\nColor: " + triangle.getColor()
                + "\nSides lengths: " + triangle.getFirst_side()
                + ", " + triangle.getSecond_side()
                + ", " + triangle.getThird_side()
                + "\nArea: " + triangle.area());

        Circle circle = new Circle(5);
        circle.setColor("Blue");
        System.out.println("Circle"
                + "\nColor: " + circle.getColor()
                + "\nRadius: " + circle.getRadius()
                + "\nArea: " + circle.area());

    }
}
