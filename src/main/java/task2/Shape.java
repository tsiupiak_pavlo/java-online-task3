package task2;

public abstract class Shape {

    String color = "Transparent";


    public Shape() {
    }

    public Shape(String color) {
        this.color = color;
    }

    public abstract double area();

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
