package task1;

public class Parrot extends Bird {

    public Parrot() {
    }

    public Parrot(String name) {
        super(name);
    }

    public Parrot(String name, int flight_altitude) {
        super(name, flight_altitude);
    }

    @Override
    public void sleep() {
        System.out.println("Parrots sleep");
    }

    @Override
    public void eat() {
        System.out.println("Parrots eat plants");
    }

    @Override
    public void fly() {
        System.out.println("Parrots fly");
    }

}
