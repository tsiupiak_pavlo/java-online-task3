package task1;

public class Main {

    public static void main(String[] args) {

        Eagle firstEagle = new Eagle("firstEagle", 100);
        System.out.println(firstEagle.getName() + " have flight altitude " + firstEagle.getFlight_altitude() + " and");
        firstEagle.sleep();
        firstEagle.eat();
        firstEagle.fly();

        Parrot firstParrot = new Parrot("firstParrot", 20);
        System.out.println(firstParrot.getName() + " have flight altitude " + firstParrot.getFlight_altitude() + " and");
        firstParrot.sleep();
        firstParrot.eat();
        firstParrot.fly();

    }
}
