package task1;

public abstract class Bird extends Animal {
    String name;
    int flight_altitude;

    public Bird() {

    }

    public Bird(String name) {
        this.name = name;
    }

    public Bird(String name, int flight_altitude) {
        this.name = name;
        this.flight_altitude = flight_altitude;
    }


    public abstract void fly();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getFlight_altitude() {
        return flight_altitude;
    }

    public void setFlight_altitude(int flight_altitude) {
        this.flight_altitude = flight_altitude;
    }
}
