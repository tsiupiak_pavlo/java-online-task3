package task1;

public abstract class Fish extends Animal {
    String name;

    public Fish() {
    }

    public Fish(String name) {
        this.name = name;
    }

    public abstract void swim();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
