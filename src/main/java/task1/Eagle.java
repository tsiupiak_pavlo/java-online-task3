package task1;

public class Eagle extends Bird {

    public Eagle() {
    }

    public Eagle(String name) {
        super(name);
    }

    public Eagle(String name, int flight_altitude) {
        super(name, flight_altitude);
    }

    @Override
    public void sleep() {
        System.out.println("Eagles sleep");
    }

    @Override
    public void eat() {
        System.out.println("Eagles eat meat");
    }

    @Override
    public void fly() {
        System.out.println("Eagles fly");
    }

}
